package com.yinsin.jpabatis.exceptions;

public class ResultMapException extends PersistenceException {
	private static final long serialVersionUID = -2541942608966159517L;

	public ResultMapException() {
	}

	public ResultMapException(String message) {
		super(message);
	}

	public ResultMapException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResultMapException(Throwable cause) {
		super(cause);
	}
}
