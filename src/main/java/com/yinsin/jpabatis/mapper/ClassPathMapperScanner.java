package com.yinsin.jpabatis.mapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import com.yinsin.jpabatis.builder.xml.XMLMapperBuilder;
import com.yinsin.jpabatis.config.Configuration;
import com.yinsin.jpabatis.config.JpaBatisProperties;
import com.yinsin.jpabatis.util.CommonUtils;

public class ClassPathMapperScanner {
	private static final Logger logger = LoggerFactory.getLogger(ClassPathMapperScanner.class);

	private JpaBatisProperties properties;
	private Configuration configuration;

	public ClassPathMapperScanner(JpaBatisProperties properties, Configuration configuration) {
		this.properties = properties;
		this.configuration = configuration;
	}

	public void parseMapper() throws Exception {
		String mapperPackage = properties.getMapper();
		if (CommonUtils.isBlank(mapperPackage))
			return;
		File dirs = ResourceUtils.getFile(mapperPackage);
		File[] xmlFiles = dirs.listFiles();
		parseXmlFile(xmlFiles);
	}

	private void parseXmlFile(File[] xmlFiles) {
		if (null == xmlFiles || xmlFiles.length == 0) {
			return;
		}
		InputStream inputStream = null;
		XMLMapperBuilder mapperParser = null;
		String resource = null;
		/*String resourceLocation = properties.getMapper();
		if (resourceLocation.startsWith(ResourceUtils.CLASSPATH_URL_PREFIX)) {
			resourceLocation = resourceLocation.substring(ResourceUtils.CLASSPATH_URL_PREFIX.length());
		}*/
		for (File file : xmlFiles) {
			if (file.isDirectory()) {
				parseXmlFile(file.listFiles());
			} else if(file.getName().endsWith(".xml")){
				try {
					logger.debug("jpabatis -> parse " + file.getName());
					inputStream = new FileInputStream(file);
				} catch (Exception e) {
					inputStream = null;
				}
				if (null != inputStream) {
					resource = file.getAbsolutePath();
					resource = resource.replaceAll("\\\\", "/");
					//resource = resource.substring(resource.indexOf(resourceLocation));
					resource = resource.substring(resource.indexOf(properties.getMapper()));
					mapperParser = new XMLMapperBuilder(inputStream, configuration, resource, configuration.getSqlFragments());
					mapperParser.parse();
				}
			}
		}
	}

	public List<String> getClassName(String packageName) {
		String filePath = ClassLoader.getSystemResource("").getPath() + packageName.replace(".", "\\");
		List<String> fileNames = getClassName(packageName, filePath, null);
		return fileNames;
	}

	private List<String> getClassName(String packageName, String filePath, List<String> className) {
		List<String> myClassName = new ArrayList<String>();
		File file = new File(filePath);
		File[] childFiles = file.listFiles();
		if (null != childFiles) {
			for (File childFile : childFiles) {
				if (childFile.isDirectory()) {
					myClassName.addAll(getClassName(packageName, childFile.getPath(), myClassName));
				} else {
					String childFilePath = childFile.getPath();
					childFilePath = childFilePath.replace("\\", ".");
					childFilePath = childFilePath.substring(childFilePath.indexOf(packageName), childFilePath.lastIndexOf("."));
					myClassName.add(childFilePath);
				}
			}
		}
		return myClassName;
	}

}
